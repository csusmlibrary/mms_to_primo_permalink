var REQUEST_DATA, REQUEST_URL;
var DELAY_MEAN = 500;
var DELAY_SD = 1000;
var DELAY_GROUP_SIZE = 5; 
var ALL_REQUESTS;
var ALL_TIMERS;

function cheapGaussian() {
    return ((Math.random() + Math.random() + Math.random() + Math.random() + Math.random() + Math.random()) - 3) / 3;
};
function delayRequest(i) {
    var r = DELAY_MEAN * (i / DELAY_GROUP_SIZE) + DELAY_SD * cheapGaussian();
    if(r < 0)
        return 0;
    else
        return Math.floor(r);
};

function parseMMS() {
    ALL_REQUESTS = new Array();
    ALL_TIMERS = new Array();

    REQUEST_URL = document.getElementById('param_gateway').value;
    REQUEST_DATA = {
        apikey: document.getElementById('param_apikey').value,        
        lang: document.getElementById('param_apikey').value,
        offset: document.getElementById('param_offset').value,
        limit: document.getElementById('param_limit').value,
        view: document.getElementById('param_view').value,
        addfields: document.getElementById('param_addfields').value,
        vid: document.getElementById('param_vid').value,
        scope: document.getElementById('param_scope').value,
    };

    var textarea = document.getElementById('mms_container');
    var inputs = textarea.value.trimRight().replace(/\r\n/g, '\n').split('\n');

    var table = $('#output_table');
    var headers = table.find('#header_row').clone();    
    table.empty();
    headers.appendTo(table);

    table.parent().show();
    
    /* create spinner */
    var ajax_spinner = new Spinner().spin();
    document.getElementById('form_container').appendChild(ajax_spinner.el);

    for(var i=0,total=inputs.length; i<total; i++) {
        var mms_id = inputs[i];
        var row = $('<tr>');
        row.append('<td class="status">');
        row.append('<td class="mms">');
        row.append('<td class="pnx">');
        row.append('<td class="permalink">');
        row.appendTo(table);

        if(mms_id.length == 0) {
            row.find('td.mms').text('(blank)');
        }
        else if(!validateMMSID(mms_id)) {
            row.find('td.mms').text(mms_id);
            row.find('td.pnx').text('Invalid MMS ID');
            markRowStatus(row,false);
        }
        else {
            row.find('td.mms').text(mms_id);
            row.attr('id', 'row-' + mms_id);
            var timer = $.whereas( function(mms_id) { makePNXRequest(mms_id) }.bind(this,mms_id),
                                   delayRequest(i) 
                                 );
            ALL_TIMERS.push(timer);
        }

    }
    $.when.apply($, ALL_TIMERS).always(
        function() {
            $.whenAll(ALL_REQUESTS).done(function() {
                // stop spinner
                ajax_spinner.stop();
            });
        }
    );

    return false;
};

MMSID_LENGTH = 17;
MMSID_INST_ID = '1452';
function validateMMSID(mms_id) {
    if(mms_id.length != MMSID_LENGTH)
        return false;
    if(!mms_id.endsWith(MMSID_INST_ID))
        return false;
    return true;
}

function makePNXRequest(mms_id) {
    REQUEST_DATA['q'] = document.getElementById('param_q_base').value + mms_id;

    var request = 
        $.getJSON(REQUEST_URL, REQUEST_DATA);

    request.done( function(data) { parsePNX_JSON(mms_id, data) } )
           .fail( function() { recordFail(mms_id) } );

    ALL_REQUESTS.push(request);    
};

function recordFail(mms_id) {
    var $row = $('#row-' + mms_id);
    markRowStatus($row, false);
    $row.find('td.pnx').text('Fail. Try Again.');
};

function parsePNX_JSON(mms_id, data) {
    // only grab the PNX if only one result
    var $row = $('#row-' + mms_id);

    if(data['docs'].length == 1) {
        var pnx = data['docs'][0]['pnxId'];
        var $row = $('#row-' + mms_id);
        // Status
        markRowStatus($row, true);

        // PNX
        var span = $('<span class="tooltip">' + pnx + '</span>');
        var tooltip = data['docs'][0]['title'];
        tooltip = tooltip.trim() + '. ' + data['docs'][0]['date'];
        tooltip = tooltip.trim() + '. ' + data['docs'][0]['publisher'];
        tooltip = tooltip.trim() + '.';
        span.append('<span>' + tooltip + '</span>');
        $row.find('td.pnx').append(span);

        // Permalink
        var url = generatePermalinkURL(pnx)
        permalink = $('<a>');
        permalink.text(url);
        permalink.attr('href', url);
        permalink.attr('target', '_blank');
        $row.find('td.permalink').append(permalink);       
    }
    else if(data['docs'].length > 0) {
        markRowStatus($row, false);
        $row.find('td.pnx').text('Multiple Items Found');
    }
    else {
        markRowStatus($row, false);
        $row.find('td.pnx').text('MMS ID Not Found');
    }          
};

function generatePermalinkURL(pnx) {
    var url = $('#param_permalink_base').val();
    url += $('#param_vid').val();
    url += ':';
    url += $('#param_scope').val();
    url += ':';
    url += pnx;
    return url;
};

function markRowStatus($row, success) {
    if(success) {
        $row.addClass('success');
        $row.find('td.status').html('<span class="hidden">SUCCESS</span><span class="symbol"></span>');
    }
    else {
        $row.addClass('fail');
        $row.find('td.status').html('<span class="hidden">FAIL</span><span class="symbol"></span>');
    }
};

$(document).ready( function() {
    $('#copy').blur(function() {
        $('.tooltip span').fadeOut();
    });
    
    var clipboard = new Clipboard('#copy'); 
    clipboard.on('success', function(e) {
        $('#copy_success').fadeIn();
        $.whereas(function() { $('#copy_success').fadeOut(); }, 3000);
        e.clearSelection();
    });

    clipboard.on('error', function(e) {
        $('#copy_fail').fadeIn();
        $.whereas(function() { $('#copy_fail').fadeOut(); }, 3000);        
    });    
});