# Utility for Converting Alma MMS IDs to Primo Permalinks #

This repo contains a web form for converting Alma MMS IDs to permalinks in the associated Primo instance. This can be useful for generating links to content that you know exists in Alma but do not know its respective docID/PNXid in Primo.

## To Use ##

1. Have Ex Libris activate the [Primo PNX REST API](https://developers.exlibrisgroup.com/primo/apis/webservices/rest/pnxs)

1. On the developer network, create an application/apikey for using the PNX REST API.

1. Fill in the fixed parameters in the web form `generator.html`.

1. Enter MMS IDs into the text area on the form and *Submit*.

## How It Works ##

For each MMS ID, a REST query is made to conduct a brief search for that exact MMS ID. Assuming that exactly one result is returned, the JSON data is scanned for that item and the PNXid is extracted. This is then used to generate a permalink (current UI) given the provided VID, Search Scope, and PNXid. All of this is placed into a table for easy use. Some metadata derived from the JSON is added to hovertext for easy confirmation.

## Details Of Use ##

A few hints and gotchas to be aware of in this utility:

* Values are currently set up for University of Washington. You'll need to adjust most of the fixed parameters.

* Both VID and Scope are case-sensitive.

* To avoid being blocked by too many API calls, the utility staggers them over time. If you submit a large number of MMS IDs, you will see this stagger effect.

* The code DOES PRESERVE ORDER of the MMS IDs as you listed them in the text area. It will even include blank rows as necessary.


## LICENSE ## 

This code is released under the OpenBSD license:

Copyright (c) 2016 Katherine Deibel

Permission to use, copy, modify, and distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.